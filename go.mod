module gitlab.com/okotek/okoclass

replace classifierFunctions v0.0.0 => ./classifierFunctions

go 1.13

require (
	classifierFunctions v0.0.0
	gitlab.com/okotek/okoframe v0.0.0-20211106035311-149369c29e05
	gitlab.com/okotek/okonet v0.0.0-20211106032734-e4b07b8b93fb
	gocv.io/x/gocv v0.28.0
)
